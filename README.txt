Credi Card On Delivery README

This project provides a Credit Card on Delivery payment method for the
Ubercart module. This payment option allows customers the flexibility
they get with Cash on Delivery.

Why Credit Card on Delivery Payment Method:
Credit cards are currently being widely used and is a good credit tool
available if used properly. The use of credit card on online transactions
are very minimal compared to in shop use. There are 2 primary reasons for
the huge gap in use. The first and foremost is the Cash on Delivery payment
option gives customers the ability to make the payment on receipt.
This offers the customers to change their mind till the product is delivered.
The second reason is for security reasons. With the amount of credit card
fraud happening online/offline customers fear providing credit card
information online in spite of all the security measures taken by the online
shopping websites.

Having understood the customer reasons for limited use of credit card on
online, many e-commerce sites may or will implement this payment method.
The customer don’t have to pay till they see the product and are convinced
with the product. Secondly no credit card information is entered online.
The delivery staff swipes the card in front of you and give the card receipt.

How does credit card on delivery work:
The delivery staff carry a small device which can be attached to a smart
phone. The delivery staff swipes the credit card on the device which is then
validated using the 4G connection from the smart phone. Once the credit card
payment is validated with the credit card issuing company, the payment is
authorized. The payment is very simple. The transaction and credit card
information are all encrypted using the highest industry standards set up
the credit agency.
